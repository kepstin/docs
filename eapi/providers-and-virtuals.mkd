Title: Providers and Virtuals
CSS: /css/main.css

#{include head}

# Providers and Virtuals

## Overview

Providers and virtuals are concepts we use in Exheres in order to make
the usage of packages which provide applications, libraries, etc.,
that are of the same general functionality easier in terms of packages'
dependencies. This documentation will attempt to dispell some common
misunderstandings about the two concepts, and hopefully make it easier
in the future for people who wish for more options to be available, to
do so without much issue.

## Rationale of providers

Users often want a choice; considering Exherbo is a source-based
distribution, we should provide a choice when it makes sense to.

Providers are likely one of the best examples of the choices that users
gain the ability to make with source-based distributions, because it
eschews the idea of hard-dependencies on certain programs in favor of
making packages depend on things for the libraries and programs they
provide. It promotes diversity within the system as well, and reduces the
likelyhood of a monoculture when it comes to the programs people use.

Not all users want to use `ffmpeg` or `libav`, so we delegate that choice
to providers, by allowing users to specify it as options...

    */* providers: -ffmpeg libav

And all packages which can use either would be smart enough to know what
the user wants to compile with. There's no arbitrary dependency on a
certain program's version of the ABI, and there's no need to edit every
single package to account for that dependency, as it is on some other
distributions.

## Rationale of virtual packages

Virtuals are a subtly different beast. Virtuals are used when it is
possible to replace a package (ex. `mariadb` -> `mysql`) without much
issue concerning ABI or linking.

For example, `pkg-config` and `pkgconf`. Both provide a `pkg-config` binary
which are generally compatible, with each other. Almost all programs will
do just fine with `pkgconf` if they do fine with `pkg-config`. Therefore,
we allow users the choice between the two, and don't make packages depend
on one of them unless it is absolutely needed.

This means that users gain the ability to put things in their
`options.conf` like...

    */* providers: -pkg-config pkgconf

and then, any package which requires `pkg-config` can look to
`virtual/pkg-config`...

    DEPENDENCIES="
        build+run:
            virtual/pkg-config
    "

then `virtual/pkg-config` lists in its dependencies...

    DEPENDENCIES="
        build+run:
            providers:pkg-config?  ( dev-util/pkg-config )
            providers:pkgconf? ( dev-util/pkgconf )
    "

and the author of the package which needs `pkg-config` doesn't need to care
about it.

The virtual satisfies the dependency for that package accordingly, and
users are free to change the provider as they please.

## Cases when virtuals don't work

Packages such as `imagemagick` and `graphicsmagick` both provide
similar ABIs, and for the most part are compatible, but they require
different libaries to be loaded, may provide different APIs, and aren't
going to be entirely compatible. This is why there is not a
`virtual/imagemagick` package; they are not compatible.

Virtuals can be useful when the provider isn't a hard dependency of the
package requiring it (via linking, or using APIs that are specific to
an implementation of a library), but in most other cases they are not
a good idea.

In these cases, coming back to the `imagemagick` example, you'd want to
use providers without virtuals instead.

`media-gfx/inkscape` has support for both and can be configured for whichever
at compile time, therefore a user can specify in their `options.conf`:

    media-gfx/inkscape providers: -imagemagick graphicsmagick

and `media-gfx/inkscape` will list in it's options:

    MYOPTIONS="
        ( providers: graphicsmagick imagemagick ) [[ number-selected = exactly-one ]]
    "
and it does what you'd expect it to do.

## Possible future plans

- Alternative `libc` implementations? glibc, musl, etc...
- Provide a `libstdc` via virtuals?; GNU's libstdc++ vs. LLVM's libc++.
- Alternative coreutils implementations with virtuals?; GNU coreutils vs. busybox.

--
Copyright 2015 Kylie McClain

#{include CC_3.0_Attribution}
#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
